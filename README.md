# Cards

This is a simple cards game written in Go to learn the basics of the language.

## Getting started
In order to run the project, run the command below in project root directory
```
go run main.go deck.go vars.go
```
## Test and Deploy
In order to run tests, run the command below in project root directory:
```
go test
```
## Contributing

## Authors and acknowledgment
This project is inspired from [Go The Complete Developer Guide](https://www.udemy.com/course/go-the-complete-developers-guide/) course on [Udemy](https://www.udemy.com/). 