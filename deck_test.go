package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	deck := newDeck()
	expectedDeckLength := 52
	expectedFirstCard := "Ace of Clubs"
	expectedLastCard := "King of Spades"

	if len(deck) != expectedDeckLength {
		t.Errorf("Expected deck length of 16, but got %v", len(deck))
	}

	if deck[0] != expectedFirstCard {
		t.Errorf("Expected first card to be Ace of Spades, but got %v", deck[0])
	}

	if deck[len(deck)-1] != expectedLastCard {
		t.Errorf("Expected last card to be Four of Clubs, but got %v", deck[len(deck)-1])
	}
}

func TestSaveToFileAndNewDeckFromFile(t *testing.T) {
	expectedDeckLength := 52
	expectedFirstCard := "Ace of Clubs"
	expectedLastCard := "King of Spades"
	testFileName := "_decktesting"
	os.Remove(testFileName)

	deck := newDeck()
	deck.saveToFile(testFileName)

	loadedDeck := newDeckFromFile(testFileName)
	if len(loadedDeck) != expectedDeckLength {
		t.Errorf("Expected length to be 16, but got %v", len(loadedDeck))
	}

	if loadedDeck[0] != expectedFirstCard {
		t.Errorf("Expected first card to be Ace of Spades, but got %v", loadedDeck[0])
	}

	if loadedDeck[len(loadedDeck)-1] != expectedLastCard {
		t.Errorf("Expected last card to be Four of Clubs, but got %v", loadedDeck[len(loadedDeck)-1])
	}

	os.Remove(testFileName)
}
